/**
 * Copyright 2007 Dr. Matthias Laux
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ml.options;

/**
 * This interface is supposed to be implemented by all classes providing help printing 
 * capabilities.
 */
public interface HelpPrinter {

    /**
     * Return a string with the command line syntax for this option set
     * <p>
     * @param set         The {@link OptionSet} to format the output for 
     * @param leadingText The text to precede the command line
     * @param lineBreak   A boolean indicating whether the command line for the option set should
     *                    be printed with line breaks after each option or not
     * <p>
     * @return A string with the command line syntax for this option set
     */
    public String getCommandLine(OptionSet set, String leadingText, boolean lineBreak);

    /**
     * Return the help text describing the different options and data arguments
     * <p>
     * @param set The {@link OptionSet} to format the output for 
     * <p>
     * @return A string with the help text for this option set
     */
    public String getHelpText(OptionSet set);
}

