/**
 * Copyright 2007 Dr. Matthias Laux
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ml.options;

/**
 * The interface for objects which can be constrained, i. e. {@link Constraint}s can 
 * be attached to such objects. 
 */
public interface Constrainable {

    /**
     * Add a constraint to this instance. 
     * <p>
     * @param constraint The {@link Constraint} to add to the list of constraints for this instance
     */
    public void addConstraint(Constraint constraint);

    /**
     * Access all known constraints
     * <p>
     * @return A list of {@link Constraint}s for this instance
     */
    public java.util.List<Constraint> getConstraints();
}

