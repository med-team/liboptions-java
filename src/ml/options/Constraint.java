/**
 * Copyright 2007 Dr. Matthias Laux
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ml.options;

/**
 * The interface for all constraints. Custom constraints need to implement this interface.
 */
public interface Constraint {

    /**
     * Check whether a constraint is satisfied. This method can be invoked after a set of 
     * command line arguments has been analyzed such that the results are known for each 
     * option and option set.
     * <p>
     * @return A boolean to indicate whether a constraint is satisfied or not
     */
    public boolean isSatisfied();

    /**
     * Indicates whether a constraint supports a given type of {@link Constrainable}
     * <p>
     * @param constrainable 
     * @return A boolean to indicate whether this {@link Constrainable} is supported
     */
    public boolean supports(Constrainable constrainable);
}


